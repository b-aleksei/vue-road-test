import Vue from 'vue';
import VueLazyLoad from 'vue-lazyload';

import loadAsset from '@/utils/loadAsset';
import debounce from '@/utils/debounce';
import router from './router';
/*
-- Uncomment if necessary ---

import api from '@/utils/api';
import throttle from '@/utils/throttle';
import random from '@/utils/random';
import shuffle from '@/utils/shuffle';

import Scroll from '@/directives/scrolls';
import WindowResize from '@/directives/windowResize';
import Animate from '@/directives/animate';

import router from './router';
import store from './store';

import plural from '@/filters/plural.js';
*/

import './scss/_base.scss';
import App from './App.vue';

const SocialSharing = require('@/utils/share');

/*
-- Uncomment if necessary ---

const VueScrollTo = require('vue-scrollto');
Vue.directive('window-scroll', Scroll.window);
Vue.directive('element-scroll', Scroll.element);
Vue.directive('window-resize', WindowResize);

Vue.filter('plural', plural);

Vue.use(Animate);

Vue.use(VueScrollTo, {
  container: 'body',
  duration: 1000,
  easing: 'ease',
  offset: 0,
  force: true,
  cancelable: true,
  x: false,
  y: true,
});
*/

Vue.use(VueLazyLoad, {
  preLoad: 1.7,
});
Vue.use(SocialSharing);

Vue.config.productionTip = false;
Vue.prototype.$utils = {
  // api,
  // throttle,
  debounce,
  loadAsset,
  loadJSON(filename) {
    return Object.freeze(require(`@/data/${filename}`));
  },
  info: Object.freeze(require('@/data/template/common.json')),
};

const vm = new Vue({
  router,
  data() {
    return {
      store: {
        debug: false,
        state: {
          totalPoints: 0,
        },
        increasePointsAction() {
          this.state.totalPoints += 1;
          if (this.debug) console.log('увеличен totalPoints', this.state.totalPoints);
        },
        resetPointsAction() {
          this.state.totalPoints = 0;
          if (this.debug) console.log('Вызван resetPointsAction', this.state.totalPoints);
        },
      },
    };
  },
  render: (h) => h(App),
}).$mount('#app');

window.vm = vm;
